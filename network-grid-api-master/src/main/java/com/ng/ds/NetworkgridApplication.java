package com.ng.ds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
@EnableSwagger2
public class NetworkgridApplication{

	public static void main(String[] args) {
		SpringApplication.run(NetworkgridApplication.class, args);
	}
}
