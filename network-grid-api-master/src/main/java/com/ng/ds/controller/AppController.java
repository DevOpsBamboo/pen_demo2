package com.ng.ds.controller;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AppController {
	
	@RequestMapping(value = "/custom")
    public String custom() {
        return "Hello Sudha!!";
    }
	
	@RequestMapping(value="/saveEventAsXml", method=RequestMethod.POST)
	public void saveEventAsXml(HttpServletRequest request) throws ServletException, IOException {
        StringBuilder sb = new StringBuilder();
        String line;
        InputStream instream = request.getInputStream();
        BufferedReader inread = new BufferedReader(new InputStreamReader(
                instream, "UTF-8"));
        while ((line = inread.readLine()) != null) {
            sb.append(line);
        }
        String inxml = sb.toString();
        String nowTime = Long.toString(System.currentTimeMillis());
        String fileName = request.getServletContext().getRealPath("/") + "/"
                + nowTime + ".xml";
        FileOutputStream fos = new FileOutputStream(fileName);
        Writer out = new OutputStreamWriter(fos, "UTF-8");
        out.write(inxml);
        out.close();
    }

}
